from pandas import read_csv, DataFrame
from pathlib import Path

class SpectrumCSVReader:
    """A class for generating spectrum dataframes from .csv files
    
    A class which on instantiation turns a Perkin-Elmer FT-IR spectrum .csv file
    into a 2 dataframes, one for header information and one containing wavenumber
    and absorbance. A spectrum can be extracted using the extract_spectrum public
    method.

    Attributes
    ----------
    csv_file : Path

        The system path of the .csv file to be read
    headers : int, optional

        The number of headers included in the .csv file (default is 3).

    Methods
    -------
    get_spectrum : pd.DataFrame

        Return the wavenumbers and absorbances of the spectrum as a DataFrame
        object.

    get_absorbances : list

        Return a list of absorbance values, descending by wavenumber.

    get_wavenumbers : list

        Return a list of the spectrums wavenumbers in descending order.
    """

    def __init__(self, csv_file:Path):
        """
        Parameters
        ----------
        csv_file : Path

            The system path of the .csv file to be read
        """
        self.file = csv_file
        self.header_count = self.count_headers()
        spectrum_headers = []
        with open(csv_file) as file:
            for i in range(0, self.header_count):
                header_elems = file.readline().strip("\n").split(":")
                if type(header_elems) is list and len(header_elems)==2:
                    spectrum_headers.append([header_elems[0], header_elems[1].strip()])
        self.headers = dict(spectrum_headers)
        if self.header_count > 0:
            self.spectrum = DataFrame(read_csv(csv_file, skiprows=(self.header_count+1), header=0))
        elif self.header_count == 0:
            self.spectrum = DataFrame(read_csv(csv_file, skiprows=0, header=0))
        else:
            raise ValueError("`header_count` cannot be negative")        

    def get_headers(self):
        return self.headers
    
    def get_spectrum(self):
        return self.spectrum

    def get_absorbances(self): 
        """Return a list of absorbance values, descending by wavenumber."""
        spectrum_array = self.spectrum.to_numpy()
        absorbances = []
        for i in spectrum_array:
            absorbances.append(i[1])
        return absorbances

    def get_wavenumbers(self):
        """Return a list of the spectrums wavenumbers in descending order."""
        spectrum_array = self.spectrum.to_numpy()
        wavenumbers = []
        for i in spectrum_array:
            wavenumbers.append(i[0])
        return wavenumbers

    def count_headers(self):
        with open(self.file) as file:
            line = ""
            i = 0
            while line != "cm-1,A\n":
                line = file.readline()
                i += 1
        if i < 0:
            header_count = 0
        else:
            header_count = (i - 2)
        return header_count