from pymongo import MongoClient
import pymongo

class DuneClient:
    """Establishes a connection with the DUNE database when instantiated

    Uses the `pymongo` API to establish a connections with the DUNE mongoDB
    database. It also all

    Attributes
    ----------
    client : MongoClient
        
        `MongoClient` containing the connection string for the class instance

    spectrums : cursor
        
        A mongoDB cursor pointing to the spectrums collection with the query:
        {"wavenumberInfo":{"start":start, "stop":stop, "step":step}}

        (To modify the query, change the parameters in the function call
        `__get_spectrums_cursor()` in `self.spectrums`)

    Methods
    -------
    get_spectrum : BSON

        Return a cursor pointing to the next spectrum in the spectrums collection

    get_spectrum_generator : BSON

        Yields a cursor pointing to the next spectrum in the DUNE spectrums
        collection. Used to avoid repeatedly extracting the same spectrum

        __get_spectrums_cursor : BSON

        Returns a non-specific cursor in the DUNE spectrums collection
    """
    def __init__(self):
        with open("C:\keys\dune_connection_string.txt") as config_file:
            connection_string = config_file.read()
        client = MongoClient(connection_string)
        db = client["dune-test"]
        coll = db["ingestTest"]
        self.client = client
        self.spectrums = self.__get_spectrums_cursor(7800, 450, 2)
        self.db = db
        self.coll = coll

    def get_spectrum(self):
        try:
            return self.spectrums.next()
        except StopIteration:
            self.spectrums.close()
            self.spectrums = client["dune-test"]["spectrums"].find()

    def get_spectrum_generator(self):
        try:
            while True:
                spectrum = self.spectrums.next()
                yield spectrum
        except StopIteration:
            self.spectrums.close()
            self.spectrums = self.client["dune-test"]["spectrums"].find()

    def __get_spectrums_cursor(self, start, stop, step):
        return self.client["dune-test"]["spectrums"].find({"wavenumberInfo":{"start":start, "stop":stop, "step":step}})